<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Coin Flipper</title>
</head>
<body>
<h1>WELCOME TO THE COIN FLIPPER</h1>

	<div>
		<h2>${bannerForPreviousGame}</h2>
		<h2>The Guess was: ${previousGuess}.	&nbsp &nbsp &nbsp	The Result was: ${previousFlipResult}.</h2>
	</div>
	
	<div style="margin-left:20px;">
		<form method="POST" name="optionPlayHeads" action="main">   	  
		    <input type="hidden" name="userGuess" value="Heads">
		    <input type="submit" value="Guess 'Heads'">
	    </form>
	</div>
	<div style="margin-left:20px;">
		<form method="POST" name="optionPlayTails" action="main">   	  
		    <input type="hidden" name="userGuess" value="Tails">
		    <input type="submit" value="Guess 'Tails'">
	    </form>
	</div>
	<div style="margin-left:20px;">
		<form method="POST" name="optionPlayEnd" action="main">   	  
		    <input type="hidden" name="userGuess" value="EndGame">
		    <input type="submit" value="End the Game">
	    </form>
	</div>
	<div style="margin-left:20px;">
		<form method="POST" name="optionPlayReset" action="main">   	  
		    <input type="hidden" name="userGuess" value="Reset">
		    <input type="submit" value="Reset">
	    </form>
	</div>
	
	<div>
		<h2>Game Statistics</h2>
		<p>Games Played:  ${totalNumberOfFlips}	&nbsp&nbsp Winning percentage: ${totalWinningPercentage} %</p>
		<p>${totalNumberOfHeads} flips were Heads.	&nbsp&nbsp	${totalNumberOfTails} flips were Tails.</p>
	</div>
</body>
</html>