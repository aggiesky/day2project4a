<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Coin Flipper</title>
</head>
<body>
<h1>WELCOME TO THE COIN FLIPPER</h1>

	<div>
		<h2>Thanks for playing.</h2>
	</div>
		
	<div>
		<h2>Game Statistics</h2>
		<p>Games Played:  ${totalNumberOfFlips}	&nbsp&nbsp Winning percentage: ${totalWinningPercentage} %</p>
		<p>${totalNumberOfHeads} flips were Heads.	&nbsp&nbsp	${totalNumberOfTails} flips were Tails.</p>
	</div>
	<div style="margin-left:20px;">
		<form method="POST" name="optionPlayRestart" action="main">   	  
		    <input type="hidden" name="userGuess" value="Restart">
		    <input type="submit" value="Restart">
	    </form>
	</div>
	
</body>
</html>