package com.vadenent;

import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CoinFlipper {
	
	private Integer totalNumberOfFlips;
	private Integer totalNumberOfHeads;
	private Integer totalNumberOfTails;

	private Integer totalNumberOfWins;

	private String  previousGuess;
	private String  previousFlipResult;
	private Boolean previousGuessAWinner;
	
	private Integer totalWinningPercentage;
	private String  bannerForPreviousGame;
	
	
	
	public CoinFlipper() {}
	
	public CoinFlipper(HttpSession session) {
		reset(session);
	}
	
	public String getBannerForPreviousGame() {
		return this.bannerForPreviousGame;
	}
	public String getPreviousGuess() {
		return this.previousGuess;
	}
	public String getPreviousFlipResult() {
		return this.previousFlipResult;
	}
	public Integer getTotalNumberOfFlips() {
		return this.totalNumberOfFlips;
	}
	public Integer getTotalWinningPercentage() {
		return this.totalWinningPercentage;
	}
	public Integer getTotalNumberOfHeads() {
		return this.totalNumberOfHeads;
	}
	public Integer getTotalNumberOfTails() {
		return this.totalNumberOfTails;
	}

	public void reset(HttpSession session) {
		
		// Set initial values for the session scope attributes
		
		this.totalNumberOfFlips     = 0;
		this.totalNumberOfHeads     = 0;
		this.totalNumberOfTails     = 0;
		this.totalNumberOfWins      = 0;
		this.totalWinningPercentage = 0;
		
		this.previousGuess          = "Heads";	// value just needs to not be NULL
		this.previousFlipResult     = "Heads";	// value just needs to not be NULL
		this.bannerForPreviousGame  = "Good Luck!";		// value just needs to not be NULL
		this.previousGuessAWinner   = true;		// value just needs to not be NULL
		
		session.setAttribute("totalNumberOfFlips", totalNumberOfFlips);
		session.setAttribute("totalNumberOfHeads", totalNumberOfHeads);
		session.setAttribute("totalNumberOfTails", totalNumberOfTails);
		session.setAttribute("totalNumberOfWins",  totalNumberOfWins);
		session.setAttribute("totalWinningPercentage", totalWinningPercentage);
		session.setAttribute("previousGuess", previousGuess);
		session.setAttribute("previousFlipResult", previousFlipResult);
		session.setAttribute("previousGuessAWinner", previousGuessAWinner);
		session.setAttribute("bannerForPreviousGame", bannerForPreviousGame);

	}

	public String getUserRequest(HttpServletRequest request) {
				
		// "userGuess is the user action that tells us what to do next; 
		// Save it in the object and session.
			this.previousGuess = request.getParameter("userGuess");
		return this.previousGuess;
	}	

	public void flipTheCoin() {
		
		// flip the coin: 0 is Tails; 1 is Heads
		// add new flip results to the totals
		
		Integer flipResult = ThreadLocalRandom.current().nextInt(0, 2);
		if (flipResult.intValue() == 0)  previousFlipResult = "Tails";
		if (flipResult.intValue() == 1)  previousFlipResult = "Heads";
		
		this.totalNumberOfHeads = this.totalNumberOfHeads + flipResult; 
		this.totalNumberOfFlips = this.totalNumberOfFlips + 1;
		this.totalNumberOfTails = this.totalNumberOfFlips - this.totalNumberOfHeads;

		this.previousGuessAWinner =    ( this.previousGuess.contentEquals("Heads") && flipResult.equals(Integer.valueOf(1)))
									|| ( this.previousGuess.contentEquals("Tails") && flipResult.equals(Integer.valueOf(0))) ;

		// If previous guess was a winner, tally a win.
		if  (this.previousGuessAWinner)  {
			this.totalNumberOfWins =  this.totalNumberOfWins + 1;
			this.bannerForPreviousGame = "You Win!";
		} else { 
			this.previousGuessAWinner = false;
			this.bannerForPreviousGame = "You Lose!";
		}
		
		this.totalWinningPercentage = (100 * this.totalNumberOfWins) / this.totalNumberOfFlips;
						
	}
		
	public Boolean getPreviousGuessAWinner() {
		return this.previousGuessAWinner;
	}
	
}