package com.vadenent;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"", "/main"})
public class HeadsOrTails extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {
		
		// Session start sets nextPage to "InitialPage.jsp"
		// Normal playing of the game sets the nextPage to "MainPage.jsp"
		// Ending the game sets the nextPage to "EndingPage.jsp"
		// doPost sets nextPage based on the user response
		
		// Retrieve the nextPage
		HttpSession session = request.getSession();		
		String nextPage = (String) session.getAttribute("nextPage");
		
		// Retrieve the data needed to write the page
		CoinFlipper cf = new CoinFlipper();
		cf = (CoinFlipper) session.getAttribute("coinFlipper");
		
		// Put the individual pieces out in the Request scope for use by this view.
		request.setAttribute("bannerForPreviousGame", cf.getBannerForPreviousGame());
		request.setAttribute("previousGuess", cf.getPreviousGuess());
		request.setAttribute("previousFlipResult", cf.getPreviousFlipResult());
		request.setAttribute("totalNumberOfFlips", cf.getTotalNumberOfFlips());
		request.setAttribute("totalNumberOfHeads", cf.getTotalNumberOfHeads());
		request.setAttribute("totalNumberOfTails", cf.getTotalNumberOfTails());
		request.setAttribute("totalWinningPercentage", cf.getTotalWinningPercentage());

		request.getRequestDispatcher("/WEB-INF/" + nextPage).forward(request, response);

	}	// end doGet
	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {

		HttpSession session = request.getSession();

		CoinFlipper cf = new CoinFlipper();	
		cf = (CoinFlipper) session.getAttribute("coinFlipper");
		
		String userRequest = cf.getUserRequest(request);			//value in request scope - not session scope	
		
		switch (userRequest) {
			case  "Heads":
			case  "Tails":
				cf.flipTheCoin();							//flip the coin and calculate the results
				session.setAttribute("coinFlipper", cf);	  //save all of the updated results back to 
															  //the respective session scope attributes
				session.setAttribute("nextPage", "MainPage.jsp");
				break;
			case  "Reset":
			case  "Restart":
				cf.reset(session);		
				session.setAttribute("nextPage", "InitialPage.jsp");
				break;
			case  "EndGame":
				session.setAttribute("nextPage", "EndingPage.jsp");
			default:	
		}

		response.sendRedirect("/Project4a");

	}	// end doPost

}
