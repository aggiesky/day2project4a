package com.vadenent;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		
		HttpSession session =se.getSession();
		CoinFlipper cf = new CoinFlipper(session);		// Initialized by the constructor
		session.setAttribute("coinFlipper", cf);		// Save object in session scope
		
		String nextPage = "InitialPage.jsp";
		session.setAttribute("nextPage", nextPage);
	
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		
	}	
	
}
